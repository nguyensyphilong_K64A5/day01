CREATE DATABASE qlsv;

CREATE TABLE IF NOT EXISTS qlsv.dmkhoa
(
    MaKH varchar(6) PRIMARY KEY,
    TenKhoa varchar(30)
);

CREATE TABLE IF NOT EXISTS qlsv.sinhvien
(
    MaSV varchar(6) PRIMARY KEY,
    HoSV varchar(30),
	TenSV varchar(15),
	GioiTinh char(1),
	NgaySinh DateTime,
	NoiSinh varchar(50),
	DiaChi varchar(50),
	MaKH varchar(6),
	HocBong int
);

INSERT INTO qlsv.sinhvien (MaSV, HoSV, TenSV, GioiTinh, NgaySinh, NoiSinh, DiaChi, MaKH, HocBong)
VALUES
    (1, 'Nguyen', 'Loong', 'M', 20010908, 'Hanoi', 'Hanoi', 'CNTT', 1),
    (2, 'Le', 'Thanh', 'M', 19951208, 'HCM', 'Hanoi', 'CNTT', 2),
    (3, 'Doo', 'Chung', 'M', 19900208, 'Danang', 'VN', 'Sinh', 3),
    (4, 'Dinh Nho', 'Hao', 'M', '19970915', 'Haiphong', 'VN', 'Hoá', 0);